---
title: "HW3"
author: "Ruide Li"
date: "2022-11-23"
output: pdf_document
---

```{r}
library(tinytex)
```

# gitlab url: https://gitlab.com/ruideli/hw3.git

# Question 1

## (1)

### My system is Windows, so the global (or user) and project (or local) level git configuration files are located under "C:\\Users\\ruide".
### File names are ".gitconfig" and ".gitconfig_local".

### The global level git configuration file is:

![global level git configuration file](global_level_git_configuration_file.png)

### The .gitignore file is:

![global level git configuration file](the_.gitignore_file.png)

## (2)

```{r}
data1 <- read.csv("2020_Business_Academic_QCQ.txt")
```

```{r}
f1 <- function(n1,n2){
  d1 <- data.frame(state = data1$State,
                   county_code = data1$County.Code, 
                   employee_size = data1$Employee.Size..5....Location,
                   sales_volume = data1$Sales.Volume..9....Location,
                   census_tract = data1$Census.Tract)
  d2 <- d1[n1:(n2-1),]
  return(d2)
}
```

## (3)

```{r}
df0 <- f1(1,300001)
census_tract_level <- unique(df0$census_tract)
sum_employee_size <- c()
sum_sales_volume <- c()
for (c in census_tract_level){
  em <- df0$employee_size[which(df0$census_tract == c)]
  em[which(is.na(em))] <- 0
  sum_employee_size <- c(sum_employee_size,sum(em))
  vo <- df0$sales_volume[which(df0$census_tract == c)]
  vo[which(is.na(vo))] <- 0
  sum_sales_volume <- c(sum_sales_volume,sum(vo))
}

df1 <- data.frame(census_tract = census_tract_level, sum_of_employee_size = sum_employee_size, sum_of_sales_volume = sum_sales_volume)
head(df1)
```

## (4)

```{r}
library(DBI) 
library(RMySQL) 
library(RJDBC)
suppressWarnings(library(tidyverse))
suppressMessages(library(dbplyr))
```

```{r}
host_name = "localhost"
port_no = 3306 
db_name = "hw3db"
u_id = "root"
pw = "Paper311"
mydrv <- dbDriver("MySQL")
conn <- dbConnect(mydrv,
dbname = db_name,
host = host_name,
port = 3306,
user = u_id,
password = pw)
```

```{r}
dbWriteTable(conn, "table1", df1)
```

```{r}
dbReadTable(conn, "table1") %>% head(6)
```

## (5)

```{r}
query_q5 <- dbSendQuery(conn, "SELECT sum_of_sales_volume, census_tract FROM table1 ORDER BY sum_of_sales_volume DESC LIMIT 10")
df_q5 <- dbFetch(query_q5)
df_q5
```

## (6)

### After making the first commit and creating the new branch, we can see:

![the first commit and the new branch](commit_and_branch_in_question_6.png)

## (7)

```{r}
source("Question 7.R")
```

```{r}
head(df2)
```

## (8)

```{r}
dbWriteTable(conn, "table2", df2)
```

```{r}
dbReadTable(conn, "table2") %>% head(6)
```

## (9)

### After making another commit, we can see:

![the second commit and git log](commit _in_question_9.png)

### The HEAD indicates the last commit we made in the current checkout branch

## (10)

```{r}
library(tidycensus)
suppressMessages(census_api_key("f75f661e969c6cca0e0d14894ead7d5579175fba",
                                install = TRUE,overwrite = TRUE))
```

```{r}
readRenviron("~/.Renviron")

suppressMessages(racial_composition <- get_decennial(
  geography = "tract", 
  variables  = c("P008003","P008004","P008005","P008006","P008007","P008008","P008009","P003001"),
  year = 2010,
  state = "AL",
))
```

```{r}

one_race_White <- racial_composition$value[1:1181]
one_race_Black_or_African_American <- racial_composition$value[1182:2362]
one_race_American_Indian_and_Alaska_Native <- racial_composition$value[2363:3543]
one_race_Asian <- racial_composition$value[3544:4724]
one_race_Native_Hawaiian_and_Other_Pacific_Islander <- racial_composition$value[4725:5905]
one_race_Other_Race <- racial_composition$value[5906:7086]
two_or_more_races <- racial_composition$value[7087:8267]
total <- racial_composition$value[8268:9448]
CT <- as.numeric(substr(racial_composition$GEOID, 6,11))[1:1181]
df3 <- data.frame(census_tract = CT,
                  one_race_White = one_race_White,
                  one_race_Black_or_African_American = one_race_Black_or_African_American,
                  one_race_American_Indian_and_Alaska_Native = one_race_American_Indian_and_Alaska_Native,
                  one_race_Asian = one_race_Asian,
                  one_race_Native_Hawaiian_and_Other_Pacific_Islander = one_race_Native_Hawaiian_and_Other_Pacific_Islander,
                  one_race_Other_Race = one_race_Other_Race,
                  two_or_more_races = two_or_more_races,
                  total = total)
```

```{r}
dbWriteTable(conn, "table3", df3)
```

```{r}
dbReadTable(conn, "table3") %>% head(6)
```

## (11)

```{r}
query_q11 <- dbSendQuery(conn, "SELECT table2.census_tract, table2.min_household_wealth, table2.Qu1st_household_wealth,
                         table2.median_household_wealth,table2.mean_household_wealth,
                         table2.Qu3st_household_wealth, table2.max_household_wealth, 
                         table2.min_household_income, table2.Qu1st_household_income,
                         table2.median_household_income,table2.mean_household_income,
                         table2.Qu3st_household_income, table2.max_household_income, 
                         table2.min_home_value, table2.Qu1st_home_value,
                         table2.median_home_value,table2.mean_home_value,
                         table2.Qu3st_home_value, table2.max_home_value,
                         table1.sum_of_employee_size, table1.sum_of_sales_volume,
                         table3.one_race_White, table3.one_race_Black_or_African_American,
                         table3.one_race_American_Indian_and_Alaska_Native, table3.one_race_Asian, 
                         table3.one_race_Native_Hawaiian_and_Other_Pacific_Islander, 
                         table3.one_race_Other_Race, table3.two_or_more_races,table3.total FROM table2 
                         JOIN table1 ON table2.census_tract = table1.census_tract 
                         JOIN table3 ON table2.census_tract = table3.census_tract")
df_q11 <- dbFetch(query_q11)
```

```{r}
head(df_q11)
```

### Two key variables I want to control is the ratio of the number of whites to the number of blacks in the level of census tract, and the number of whites to the number of asians in the level of census tract.

## (12)

### After making another commit, we can see:

![the commit and git log](question_12_commit.png)

### the merge step:

![the merge](question_12_merge.png)

### We can reset the repository to the older version by typing "git reset --hard 0ad5a7a6" in the terminal

## (13)

```{r}
ratio1 <- df_q11$one_race_White/df_q11$total
ratio2 <- df_q11$one_race_Black_or_African_American/df_q11$total
ratio3 <- df_q11$one_race_Asian/df_q11$total
ratio4 <- df_q11$one_race_American_Indian_and_Alaska_Native/df_q11$total
ratio5 <- df_q11$one_race_Native_Hawaiian_and_Other_Pacific_Islander/df_q11$total
ratio6 <- df_q11$one_race_Other_Race/df_q11$total
CT_model <- lm(df_q11$median_home_value~ratio1+ratio2+ratio3+ratio4+ratio5+ratio6)
```

```{r}
summary(CT_model)
```

```{r}
par(mfrow = c(2, 2))
plot(CT_model)
```

### I created an MLR model at census tract level. After checking diagonal plots. According to the Residuals vs Fitted Plot, the red line is across the plot horizontally, then we can assume that residuals follow a linear pattern(linearity is good). According to the Normal Q-Q Plot, we can see that there most of points in this plot that do not distribute along a straight diagonal line, so we can assume that residuals are not normally distributed well. According to the Scale-Location Plot, the red line is not horizontal across the plot, then we can assume the assumption of constant variance is not true. According to the Residuals vs Leverage Plot Since there is few points are outside of Cook's distance, then we can say there is few influential points. Besides, the summary show that the value of r square is only 0.3887. It indicates that the ratio of median of home value that can be explained from ratio of different races is low. So we can say that there exists racial bias in home evaluation, but not serious.


# Question 2

## (1)

### A nodes is a computer part of cluster, which provides resources like volatile memory (e.g. RAM), processors, permanent disk space (e.g. SSD), accelerators (e.g. GPU) and so on. A core is the part of a processor which does the computation.

### Users interact with an HPC cluster through login nodes. Login nodes are a place where users can login, edit files, view job results and submit new jobs. Login nodes are a shared resource and should not be used to run application workloads. An HPC cluster is made up of a number of compute nodes, each compute node has a complement of processors, memory and GPUs.

## (2)

### the SLURM script:

![the SLURM script](question2_2.png)

## (3)

### the full path to my scratch directory is E:\506\hw3. The original directory remains unaffected when the scratch link is deleted.


