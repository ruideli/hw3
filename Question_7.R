# The R script for question 7

data2 <- read.csv("AL.csv")
raw_data <- data.frame(household_wealth = data2$FIELD19, 
                       household_income = data2$FIELD20, 
                       home_value = data2$FIELD22,
                       state = data2$FIELD45,
                       county_code = data2$FIELD64,
                       census_tract = data2$FIELD65)
raw_data <- raw_data[-which(raw_data$home_value == 0),]
min_household_wealth <- c()
Qu1st_household_wealth <- c()
median_household_wealth <- c()
mean_household_wealth <- c()
Qu3st_household_wealth <- c()
max_household_wealth <- c()
min_household_income <- c()
Qu1st_household_income <- c()
median_household_income <- c()
mean_household_income <- c()
Qu3st_household_income <- c()
max_household_income <- c()
min_home_value <- c()
Qu1st_home_value  <- c()
median_home_value  <- c()
mean_home_value <- c()
Qu3st_home_value  <- c()
max_home_value  <- c()
for (c in unique(raw_data$census_tract)){
  
  Summarize_household_wealth <- summary(raw_data$household_wealth[which(raw_data$census_tract == c)])
  min_household_wealth <- c(min_household_wealth,as.numeric(Summarize_household_wealth[1]))
  Qu1st_household_wealth <- c(Qu1st_household_wealth,as.numeric(Summarize_household_wealth[2]))
  median_household_wealth <- c(median_household_wealth,as.numeric(Summarize_household_wealth[3]))
  mean_household_wealth <- c(mean_household_wealth,as.numeric(Summarize_household_wealth[4]))
  Qu3st_household_wealth <- c(Qu3st_household_wealth,as.numeric(Summarize_household_wealth[5]))
  max_household_wealth <- c(max_household_wealth,as.numeric(Summarize_household_wealth[6]))
  
  Summarize_household_income <- summary(raw_data$household_income[which(raw_data$census_tract == c)])
  min_household_income <- c(min_household_income,as.numeric(Summarize_household_income[1]))
  Qu1st_household_income <- c(Qu1st_household_income,as.numeric(Summarize_household_income[2]))
  median_household_income <- c(median_household_income,as.numeric(Summarize_household_income[3]))
  mean_household_income <- c(mean_household_income,as.numeric(Summarize_household_income[4]))
  Qu3st_household_income <- c(Qu3st_household_income,as.numeric(Summarize_household_income[5]))
  max_household_income <- c(max_household_income,as.numeric(Summarize_household_income[6]))
  
  Summarize_home_value <- summary(raw_data$home_value[which(raw_data$census_tract == c)])
  min_home_value <- c(min_home_value,as.numeric(Summarize_home_value[1]))
  Qu1st_home_value <- c(Qu1st_home_value,as.numeric(Summarize_home_value[2]))
  median_home_value <- c(median_home_value,as.numeric(Summarize_home_value[3]))
  mean_home_value <- c(mean_home_value,as.numeric(Summarize_home_value[4]))
  Qu3st_home_value <- c(Qu3st_home_value,as.numeric(Summarize_home_value[5]))
  max_home_value <- c(max_home_value,as.numeric(Summarize_home_value[6]))
}

df2 <- data.frame(census_tract = unique(raw_data$census_tract),
                  min_household_wealth = min_household_wealth,
                  Qu1st_household_wealth = Qu1st_household_wealth,
                  median_household_wealth = median_household_wealth,
                  mean_household_wealth = mean_household_wealth,
                  Qu3st_household_wealth = Qu3st_household_wealth,
                  max_household_wealth = max_household_wealth,
                  min_household_income = min_household_income,
                  Qu1st_household_income = Qu1st_household_income,
                  median_household_income = median_household_income,
                  mean_household_income = mean_household_income,
                  Qu3st_household_income = Qu3st_household_income,
                  max_household_income = max_household_income,
                  min_home_value = min_home_value,
                  Qu1st_home_value = Qu1st_home_value,
                  median_home_value = median_home_value,
                  mean_home_value = mean_home_value,
                  Qu3st_home_value = Qu3st_home_value,
                  max_home_value = max_home_value)







